const { Selector } = require('testcafe');
require('dotenv').config();

const url = `${process.env.BASE_URL}`;

const subjekSelect = Selector('div select.form-control.pintek-select');
const subjekOption = subjekSelect.find('option[value="Keluhan Layanan"]');

exports.navigateToThisPage = async function () {
    return testController.navigateTo(url);
};

exports.homepage = {
    async logo(){
        await testController
        .click(Selector('div a.header-logo > img'))

    },
    async Headers(){
        await testController
    },
    async buttonStuSelengkapnya(){
        await testController
        .click(Selector('a').withText('Selengkapnya'))
    },
    async institutionSection(){
        await testController

    },
    async testimoniSection(){
        await testController
        for(let i = 0; i < 20; i++) {
            await testController
            .click(Selector('button.owl-prev'))
        }
        
    },
    async mitraSection(){
        await testController
        .setTestSpeed(0.5)
        .click(Selector('#pills-pt-tab'))
        .wait(100)
        .click(Selector('#pills-kursus-tab'))
        .wait(100)
        .click(Selector('#pills-lpk-tab'))
        .wait(100)
        .click(Selector('#pills-edtech-tab'))
        .wait(100)
        .click(Selector('#pills-lk-tab'))
        .wait(100)
        .click(Selector('#pills-media-tab'))
    },
    async HubungiKami(){
        await testController
            .typeText(Selector('input[name="fullname"]'), 'Faris')
            .typeText(Selector('input[name="email"]'), 'qa@pintek.id')
            .typeText(Selector('input[name="phone_no"]'), '081385435013')
            // .click(subjekSelect)
            // .click(subjekOption)
            .typeText(Selector('#message').withText('Ini hasil dari automation testing'))
            .click(Selector('button').withText('Kirim'))
            

            
    }
}

exports.getH1Pintek = function () {
    return Selector('div.col-lg-12 > h1').with({ boundTestRun: testController }).textContent;
};

exports.getTitleCalculator = function () {
    return Selector('div.title').with({ boundTestRun: testController }).textContent;
};

exports.getTitleTestimoni = function () {
    return Selector('h1.section-title.text-center.f-bright').with({ boundTestRun: testController }).textContent;
};

exports.getTitleMitra = function () {
    return Selector('div.section.partner-section .section-title.text-center.mb-5.f-primary').with({ boundTestRun: testController }).textContent;
};

exports.getTitleHubungiKami = function () {
    return Selector('div.col-md-6.offset-md-3').with({ boundTestRun: testController }).textContent;
};