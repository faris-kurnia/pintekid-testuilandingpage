const { Selector } = require('testcafe');
require('dotenv').config();

const pendidikanSelect = Selector('#borrower_education');
const pendidikanOption = pendidikanSelect.find('option[value="3"]');

const pernikahanSelect = Selector('#borrower_marital_status');
const pernikahanOption = pernikahanSelect.find('option[value="2"]');

const agamaSelect = Selector('#borrower_religion');
const agamaOption = agamaSelect.find('option[value="1"]');

const provinsiSelect = Selector('#borrower_province');
const provinsiOption = provinsiSelect.find('option[value="DKI Jakarta"]');

const kotaSelect = Selector('#borrower_district');
const kotaOption = kotaSelect.find('option[value="Jakarta Pusat"]');

const kecamatanSelect = Selector('#borrower_subdistrict');
const kecamanatanOption = kecamatanSelect.find('option[value="Kemayoran"]');

const kelurahanSelect = Selector('#borrower_village');
const kelurahanOption = kelurahanSelect.find('option[value="4835"]');

const borrowerrltn1Select = Selector('#fam_borrower_relation_1');
const borrowerrltn1Option = borrowerrltn1Select.find('option[value="2"]');

const borrowerrltn2Select = Selector('#fam_borrower_relation_2');
const borrowerrltn2Option = borrowerrltn2Select.find('option[value="5"]');

const borrowerEmpTypeSelect = Selector('#borrower_employment_type');
const borrowerEmpTypeOption = borrowerEmpTypeSelect.find('option[value="4"]');

const borrowerEmpStatSelect = Selector('#borrower_employment_status');
const borrowerEmpStatOption = borrowerEmpStatSelect.find('option[value="3"]');

const provinsiEmpSelect = Selector('#borrower_employment_province');
const provinsiEmpOption = provinsiEmpSelect.find('option[value="Jawa Barat"]');

const kotaEmpSelect = Selector('#borrower_employment_district');
const kotaEmpOption = kotaEmpSelect.find('option[value="Indramayu"]');

const kecamatanEmpSelect = Selector('#borrower_employment_subdistrict');
const kecamanatanEmpOption = kecamatanEmpSelect.find('option[value="Kedokan Bunder"]');

const kelurahanEmpSelect = Selector('#borrower_employment_village');
const kelurahanEmpOption = kelurahanEmpSelect.find('option[value="9021"]');

exports.step = {
    async personal(){
        await testController
        .typeText(Selector('#borrower_name'),'Faris KK')
        .wait(200)
        .typeText(Selector('#borrower_pob'),'Rembang')
        .wait(200)
        .typeText('#borrower_dob', '1993-04-04')
        .wait(200)
        .typeText(Selector('#borrower_phone_no'), '081385435019')
        .wait(200)
        .typeText(Selector('#borrower_email'), 'faris.qaqa@yopmail.com')
        .wait(200)
        .typeText(Selector('#borrower_personal_id'), '3317140404939091')
        .wait(200)
        .click(pendidikanSelect)
        .click(pendidikanOption.withText('SMA/SMK'))
        .wait(200)
        .click(pernikahanSelect)
        .click(pernikahanOption.withText('Lajang'))
        .wait(200)
        .typeText(Selector('#borrower_depents'), '2')
        .wait(200)
        .click(agamaSelect)
        .click(agamaOption.withText('Islam'))
        .wait(200)
        .typeText(Selector('#borrower_address'), 'Jl. Raya Pasar Minggu No.1')
        .wait(200)
        .click(provinsiSelect)
        .click(provinsiOption.withText('DKI Jakarta'))
        .wait(200)
        .click(kotaSelect)
        .click(kotaOption.withText('Jakarta Pusat'))
        .wait(200)
        .click(kecamatanSelect)
        .click(kecamanatanOption.withText('Kemayoran'))
        .wait(200)
        .click(kelurahanSelect)
        .click(kelurahanOption.withText('Utan Panjang'))
        .wait(200)
        .click(Selector('#flag_dom_address'))
        .wait(200)
        .click(Selector('input[name="home_status"]'))
        .wait(200)
        .click(Selector('input[name="borrower_student_status"]'))
        .wait(200)
        .click(Selector('#flag_borrower_student_address'))
        .wait(200)
        .click(Selector('#btn-submit-personal').withText('Simpan dan Lanjut'));
    },
    async keluarga(){
        await testController
        .typeText(Selector('input[name="borrower_mother_name"]'),'Ibu Saya') 
        await testController
        .click(borrowerrltn1Select)
        .click(borrowerrltn1Option.withText('Suami/Istri'))
        .wait(200)
        .typeText(Selector('#fam_borrower_name_1'),'Suami Ariana')
        .typeText(Selector('#fam_borrower_phone_1'),'2154324234')
        .click(borrowerrltn2Select)
        .click(borrowerrltn2Option.withText('Kakek / Nenek'))
        .wait(200)
        .typeText(Selector('#fam_borrower_name_2'),'Nenek Ariana')
        .typeText(Selector('#fam_borrower_phone_2'),'2175463653')
        .click(Selector('#btn-submit-family').withText('Simpan'));
    },
    async pekerjaan(){
        await testController
        .click(borrowerEmpTypeSelect)
        .click(borrowerEmpTypeOption.withText('Karyawan Swasta'))
        .wait(200)
        .click(borrowerEmpStatSelect)
        .click(borrowerEmpStatOption.withText('Pekerja Lepas (Freelance)'))
        .wait(200)
        .typeText(Selector('#brw_employment_name'),'Tempat Kerja Faris')
        .wait(200)
        .typeText(Selector('#borrower_emplyment_startdate'),'2015-01-13')
        .wait(200)
        .typeText(Selector('#text_employment_salary'),'54663534')
        .wait(200)
        .typeText(Selector('#borrower_employment_phone_no'),'6454324')
        .wait(200)
        .typeText(Selector('#borrower_employment_position'),'Manager')
        .wait(200)
        .typeText(Selector('input[name="borrower_employment_address"]'),'Jalan Panjang No.73A')
        .wait(200)
        .click(provinsiEmpSelect)
        .click(provinsiEmpOption.withText('Jawa Barat'))
        .wait(200)
        .click(kotaEmpSelect)
        .click(kotaEmpOption.withText('Indramayu'))
        .wait(200)
        .click(kecamatanEmpSelect)
        .click(kecamanatanEmpOption.withText('Kedokan Bunder'))
        .wait(200)
        .click(kelurahanEmpSelect)
        .click(kelurahanEmpOption.withText('Jayawinangun'))
        .wait(200)
        .click(Selector('#btn-submit-job').withText('Simpan dan Lanjut'))
    },
    async dokumen(){
        await testController
        .click(Selector('#btn_upload_file_national_id'))
        .wait(200)

        .setFilesToUpload('#file_upload', [
            __dirname+"/images/doc1.png"
        ])
        .click(Selector('#btnSimpanFile'))
        .click(Selector('#btn_upload_file_family_id'))
        .wait(200)
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/doc2.png"
        ])
        .click(Selector('#btnSimpanFile'))
        .click(Selector('#btn_upload_file_selfie_with_id'))
        .wait(200)
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/doc3.png"
        ])
        .click(Selector('#btnSimpanFile'))
        .click(Selector('#btn_upload_file_Doc2'))
        .wait(200)
        .setFilesToUpload('#file_upload', [
            __dirname+"/images/doc4.png"
        ])
        .click(Selector('#btnSimpanFile'))
    },
    async final(){
        await testController
        .click(Selector('#btn-submit-document').withText('Simpan Semuanya'))
        .wait(500)
        .click(Selector('#btn_check_status'))
    }
}