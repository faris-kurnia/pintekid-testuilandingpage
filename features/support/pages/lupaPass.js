const { Selector } = require('testcafe');
require('dotenv').config();

exports.lupaPass = {
    async email(){
        await testController
        .click(Selector('a').withText('Lupa Kata Sandi?'))

    }
}

exports.getTextLupa = function () {
    return Selector('span.text-30').with({ boundTestRun: testController }).textContent;
};