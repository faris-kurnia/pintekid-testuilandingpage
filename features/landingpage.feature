Feature: Landing Page - Web

  User can open website pintek and user can see all information about pintek

Scenario: Home student section
    Given User can open pintek landing page
    Then User see H1 Jawaban Aspirasi Pendidikan 
    When User see section student and click button selengkapnya
    Then User directed to cicilan biaya sekolah page and see Simulasi Pinjaman
      And User click logo pintek in student page


Scenario: Home testimoni section
    Given User can open pintek landing page
    Then User can see testimoni section
      And user can click arrow next and prev 


Scenario: Home mitra and partner section
    Given User can open pintek landing page
    Then User can see mitra and partner section
      And User can click all tab in section mitra and partner


# Scenario: Users can ask questions on Pintek with the contact us form
#     Given User can open pintek landing page
#     Then User can see form hubungi kami
#       And User input all form and click kirim
    
    