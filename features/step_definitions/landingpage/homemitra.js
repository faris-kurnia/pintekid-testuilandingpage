const { Then } = require('cucumber');
const mitra = require('../../support/pages/landingpage/homepage');

Then('User can see mitra and partner section', async () => {
    await testController.expect(mitra.getTitleMitra()).contains('Mitra');
});

Then('User can click all tab in section mitra and partner', async () => {
    await mitra.homepage.mitraSection();
});