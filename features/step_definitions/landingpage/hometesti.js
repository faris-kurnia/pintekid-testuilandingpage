const { Then } = require('cucumber');
const testi = require('../../support/pages/landingpage/homepage');

Then('User can see testimoni section', async () => {
    await testController.expect(testi.getTitleTestimoni()).contains('Testimoni');
});

Then('user can click arrow next and prev', async () => {
    await testi.homepage.testimoniSection();
});