const { Given, When, Then } = require('cucumber');
const neow = require('../../support/pages/landingpage/homepage');

Given('User can open pintek landing page', async () => {
    await neow.navigateToThisPage();
});

Then('User see H1 Jawaban Aspirasi Pendidikan', async () => {
    await testController.expect(neow.getH1Pintek()).contains('Jawaban Aspirasi Pendidikan');
});

When('User see section student and click button selengkapnya', async () => {
    await neow.homepage.buttonStuSelengkapnya();
});

Then('User directed to cicilan biaya sekolah page and see Simulasi Pinjaman', async () => {
    await testController.expect(neow.getTitleCalculator()).contains('Simulasi Pinjaman');
});

When('User click logo pintek in student page', async () => {
    await neow.homepage.logo();
})
